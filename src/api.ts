import { ref } from "vue";
import properties from "./assets/properties.json";

export const username = ref();

export function updateUsername(): Promise<void> {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem("token");
    username.value = null;
    if (token) {
      whoAmI(token)
        .then((user) => {
          username.value = user;
          resolve();
        })
        .catch((err) => {
          localStorage.removeItem("token");
          reject(err);
        });
    } else {
      reject();
    }
  });
}

export function logIn(user: string, password: string): Promise<void> {
  const headers = getBaseHeaders();
  headers.append("Authorization", "Basic " + btoa(user + ":" + password));

  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/users/credentials", {
      method: "POST",
      headers: headers,
    })
      .then((response) => {
        if (response.ok) return response.text();
        else if (response.status == 401) reject("Invalid credentials");
        else reject("Error while logging in");
      })
      .then((token) => {
        if (token) {
          localStorage.setItem("token", token);
          updateUsername();
          resolve();
        } else reject("Error while logging in");
      })
      .catch((err) => reject(err));
  });
}

export function logOut(): void {
  const headers = getBaseHeaders();

  fetch(properties.apiUrl + "/users/credentials", {
    method: "DELETE",
    headers: headers,
  });

  username.value = null;
  localStorage.removeItem("token");
}

export function newAccount(user: string, password: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/users", {
      method: "POST",
      headers: getBaseHeaders(),
      body: JSON.stringify({ name: user, password: password }),
    })
      .then((response) => {
        if (response.ok) resolve();
        else if (response.status == 401) reject("Account alredy exists");
        else reject("Error while creating account");
      })
      .catch((err) => reject(err));
  });
}

export function deleteAccount(): Promise<void> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/users", {
      method: "DELETE",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) {
          username.value = null;
          localStorage.removeItem("token");
          resolve();
        } else reject("Error while deleting account");
      })
      .catch((err) => reject(err));
  });
}

export function getRandomPost(): Promise<Post> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts/random", {
      method: "GET",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve(response.json());
        else reject("Error while getting a post");
      })
      .catch((err) => reject(err));
  });
}

export function newPost(content: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts", {
      method: "POST",
      headers: getBaseHeaders(),
      body: JSON.stringify({ content: content }),
    })
      .then((response) => {
        if (response.status == 201) resolve();
        else if (response.status == 400) reject("Invalid request");
        else reject("Error while creating new post");
      })
      .catch((err) => reject(err));
  });
}

export function getPosts(): Promise<Post[]> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts", {
      method: "GET",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve(response.json());
        else reject("Error while getting posts");
      })
      .catch((err) => reject(err));
  });
}

export function deletePost(id: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts/" + id, {
      method: "DELETE",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve();
        else reject("Error while deleting post");
      })
      .catch((err) => reject(err));
  });
}

export function votePost(id: string, up: boolean): Promise<void> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts/" + id + "/votes?up=" + up, {
      method: "POST",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve();
        else reject("Error while sending vote");
      })
      .catch((err) => reject(err));
  });
}

export function getVotes(): Promise<Vote[]> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts/votes", {
      method: "GET",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve(response.json());
        else reject("Error while getting votes");
      })
      .catch((err) => reject(err));
  });
}

export function deleteVote(id: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/posts/" + id + "/votes", {
      method: "DELETE",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve();
        else reject("Error deleting vote");
      })
      .catch((err) => reject(err));
  });
}

function getBaseHeaders() {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");

  const token = localStorage.getItem("token");
  if (token) headers.append("Authorization", "Bearer " + token);

  return headers;
}

function whoAmI(token: string): Promise<string> {
  return new Promise((resolve, reject) => {
    fetch(properties.apiUrl + "/users/credentials", {
      method: "GET",
      headers: getBaseHeaders(),
    })
      .then((response) => {
        if (response.ok) resolve(response.text());
        else if (response.status == 401) reject("Invalid token");
        else reject("Error whoami");
      })
      .catch((err) => reject(err));
  });
}
