type Post = {
  id: string;
  username: string;
  content: string;
  score: number;
};
